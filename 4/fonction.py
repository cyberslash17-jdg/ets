def isinteger(a):
    try:
        int(a)
        return True
    except ValueError:
        return False

def fonction(string):
	carac_array = list(string)
		
	string_return = ""
	
	for carac in carac_array:
		
		if not carac.istitle():
			
			if isinteger(carac) == True:
				string_return = carac + string_return	
			
			elif(carac == " "):		
				string_return = string_return + " "
			
			elif(carac == "a"):		
				string_return = string_return + "a"
				
			elif(carac == "b"):		
				string_return = string_return + "b"		
			
			elif(carac == "c"):		
				string_return = string_return + "c"
				
			elif(carac == "d"):		
				string_return = "d" + string_return	
				
			elif(carac == "e" or carac == "l"):		
				string_return = string_return + "e"			
			
			elif(carac == "f"):		
				string_return = string_return + "f"	
			
			elif(carac == "g" or carac == "n"):		
				string_return = "g" + string_return	
				
			elif(carac == "h"):		
				string_return = "h" + string_return	
				
			elif(carac == "i"):		
				string_return = "b" + string_return	
				
			elif(carac == "j"):		
				string_return = "c" + string_return				
			
			elif(carac == "k"):		
				string_return = "d" + string_return	
			
			elif(carac == "m"):		
				string_return = "f" + string_return	
				
			elif(carac == "o"):		
				string_return = string_return + "h"
				
			elif(carac == "p"):		
				string_return = string_return + "i"			
			
			elif(carac == "q"):		
				string_return = "j" + string_return	
				
			elif(carac == "r"):		
				string_return = string_return + "k"	
			
			elif(carac == "s"):		
				string_return = string_return + "l"	
			
			elif(carac == "t"):		
				string_return = string_return + "m"	
			
			elif(carac == "u"):		
				string_return = string_return + "n"	
			
			elif(carac == "v"):		
				string_return = "o" + string_return	
			
			elif(carac == "w"):		
				string_return = "p" + string_return
			
			elif(carac == "x"):		
				string_return = "q" + string_return
			
			elif(carac == "y"):		
				string_return = "r" + string_return
			
			elif(carac == "z"):		
				string_return = "s" + string_return					
			
			else:
				string_return = carac + string_return
		
		elif carac.istitle():
			
			if(carac == "B"):
				string_return = string_return + carac	
			
			else:
				string_return = carac + string_return		
		
	return string_return

