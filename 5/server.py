#!/usr/bin/env python

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
from sympy.utilities.iterables import multiset_permutations
import numpy as np
import json

def getPerm(arr):
    x = []
    for p in multiset_permutations(arr):
        x.append(' '.join(p))
    return x
	
class S(BaseHTTPRequestHandler):
    def do_POST(self):
                
        request_path = self.path        
        print("\n----- Request Start ----->\n")
        print(request_path)

        request_headers = self.headers
        content_length = request_headers.getheaders('content-length')
        length = int(content_length[0]) if content_length else 0
        if request_path == '/permutations':            
            try:
                data = self.rfile.read(length) 
                print data.split(' ')
                print np.array(data.split(' '))
                content = json.dumps({ "result" : getPerm(np.array(data.split(' '))) })
                print content
                
                self.send_header("Content-Length", len(content))
                self.send_header("Content-Type", "application/json")
                self.end_headers()
                self.wfile.write(content)
                self.send_response(200)
                print("<----- Request End -----\n")
            except Exception as e:
                print e
                
        self.send_response(200)
        
def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv
    run()